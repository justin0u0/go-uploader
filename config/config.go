package config

import (
	"os"

	flags "github.com/jessevdk/go-flags"
	"github.com/sirupsen/logrus"
)

type ArgsOptions struct {
	Token       string `short:"t" long:"token" desciption:"The access token of the user"`
	LoggerLevel string `long:"logger_level" description:"Configure logrus logger level" default:"DEBUG"`
	MountPath   string `short:"m" long:"mount_path" description:"The path where file is uploaded" default:"upload"`
}

var Args ArgsOptions

var log = logrus.StandardLogger().WithField("package", "config")

func init() {
	// parse arguments
	parser := flags.NewParser(&Args, flags.Default)
	if _, err := parser.Parse(); err != nil {
		log.Errorln("Failed to parse os.Args:", err.Error())
		panic("failed to parse os.Args")
	}

	// setup logger
	level, err := logrus.ParseLevel(Args.LoggerLevel)
	if err != nil {
		log.Errorln("Failed to parse logger level:", err.Error())
		panic("failed to parse logger level")
	}
	logrus.SetLevel(level)

	// create upload directory
	if err := os.MkdirAll(Args.MountPath, os.ModePerm); err != nil {
		log.Errorln("Failed to create upload directory", err.Error())
		panic("failed to create upload directory")
	}

	log.Debugln("parse args ", "token:", Args.Token, "logger_level:", Args.LoggerLevel, "mount_path:", Args.MountPath)
}
