FROM golang:1.16-alpine
WORKDIR /app
COPY go.mod .
COPY go.sum .
RUN go mod download
COPY . .
RUN go build -o go-uploader .

FROM alpine
WORKDIR /app
COPY --from=0 /app/go-uploader ./go-uploader
EXPOSE 8181
ENTRYPOINT ["./go-uploader"]
