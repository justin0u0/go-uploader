package main

import (
	"context"
	"errors"
	"net/http"
	"time"

	"gitlab.com/justin0u0/go-uploader/routes"

	"github.com/sirupsen/logrus"
)

var log = logrus.StandardLogger().WithFields(logrus.Fields{
	"package": "main",
	"module":  "main",
})

func main() {
	// done signal file upload done and can be terminated
	done := make(chan bool)

	router := routes.InitializeRouter(done)

	svc := &http.Server{
		Addr:    ":8181",
		Handler: router,
	}

	go func() {
		if err := svc.ListenAndServe(); err != nil && errors.Is(err, http.ErrServerClosed) {
			log.Infoln("listen: %s\n", err)
		}
	}()

	<-done

	ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
	defer cancel()

	if err := svc.Shutdown(ctx); err != nil {
		log.Fatal("server forced to shutdown:", err)
	}

	log.Infoln("server gracefully shutdown")
}
