package routes

import (
	"net/http"
	"time"

	"gitlab.com/justin0u0/go-uploader/controllers"
	"gitlab.com/justin0u0/go-uploader/middlewares"

	"github.com/gin-contrib/cors"
	"github.com/gin-gonic/gin"
	"github.com/sirupsen/logrus"
)

var log = logrus.StandardLogger().WithField("package", "routes")

func InitializeRouter(done chan bool) *gin.Engine {
	router := gin.Default()

	router.Use(cors.New(cors.Config{
		AllowAllOrigins:  true,
		AllowMethods:     []string{"GET", "POST", "OPTIONS"},
		AllowHeaders:     []string{"*"},
		AllowCredentials: false,
		MaxAge:           12 * time.Hour,
	}))

	router.POST("/upload", middlewares.Authenticate(), controllers.HandleFileUpload(done))

	router.GET("/close", middlewares.Authenticate(), func(ctx *gin.Context) {
		log.Infoln("requested to close the server")
		ctx.Status(http.StatusNoContent)
		done <- true
	})

	router.GET("/healthz", func(ctx *gin.Context) {
		ctx.JSON(http.StatusOK, gin.H{"status": "ok"})
	})

	return router
}
