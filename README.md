# Introduction

Go uploader.

Allow using HTTP to upload a file then gracefully shutdown the server.

# Usage

Build and run locally:

```bash
go build -o ./bin/go-uploader
./bin/go-uploader --token "{token}"
```

Upload file:
```bash
curl -X POST -F "file=./examples/example_10m_file" \
-H "Authorization: Bearer {token}" \
http://localhost:8181/upload
```

Replace `{token}` to your token string.

# Docker Image

https://hub.docker.com/r/justin0u0/go-uploader
