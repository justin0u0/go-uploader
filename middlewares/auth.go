package middlewares

import (
	"errors"
	"net/http"
	"strings"

	"gitlab.com/justin0u0/go-uploader/config"

	"github.com/gin-gonic/gin"
	"github.com/sirupsen/logrus"
)

var log = logrus.StandardLogger().WithField("package", "middlewares")

// authHeader defines header data we need to do authentication
type authHeader struct {
	Authorization string `header:"Authorization" binding:"required"`
}

func Authenticate() gin.HandlerFunc {
	return func(ctx *gin.Context) {
		var header authHeader
		if err := ctx.ShouldBindHeader(&header); err != nil {
			ctx.AbortWithStatusJSON(http.StatusUnauthorized, gin.H{"msg": "authentication failed", "err": err})
			log.Errorln("Failed to authenticate:", err.Error())
			return
		}
		if !strings.HasPrefix(header.Authorization, "Bearer ") {
			err := errors.New("authorization JWT should starts with 'Bearer '")
			ctx.AbortWithStatusJSON(http.StatusUnauthorized, gin.H{"msg": "authentication failed", "err": err})
			log.Errorln("Failed to authenticate:", err.Error())
			return
		}

		token := header.Authorization[7:]
		log.Debugln("Authentication middleware get token:", token)

		if token != config.Args.Token {
			err := errors.New("token mismatch")
			ctx.AbortWithStatusJSON(http.StatusUnauthorized, gin.H{"msg": "authentication failed", "err": err})
			log.Errorln("Failed to authenticate:", err.Error())
			return
		}

		ctx.Next()
	}
}
