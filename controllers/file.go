package controllers

import (
	"net/http"
	"path/filepath"

	"github.com/gin-gonic/gin"
	"github.com/sirupsen/logrus"
	"gitlab.com/justin0u0/go-uploader/config"
)

var log = logrus.StandardLogger().WithField("package", "controllers")

func HandleFileUpload(done chan bool) func(*gin.Context) {
	return func(ctx *gin.Context) {
		file, err := ctx.FormFile("file")
		if err != nil {
			ctx.JSON(http.StatusBadRequest, gin.H{"msg": "failed to get form", "err": err})
			log.Errorln("Failed to get form:", err.Error())
			return
		}

		fileName := filepath.Base(file.Filename)
		dst := filepath.Join(config.Args.MountPath, fileName)
		if err := ctx.SaveUploadedFile(file, dst); err != nil {
			ctx.JSON(http.StatusBadRequest, gin.H{"msg": "failed to upload file", "err": err})
			log.Errorln("Failed to upload file:", err.Error())
			return
		}

		log.Infoln("File", file.Filename, "uploaded successfully")
		ctx.JSON(http.StatusOK, gin.H{"fileName": fileName})
		done <- true
	}
}
